import urllib
import hashlib
import os
import random
import time
import sqlite3
from bs4 import BeautifulSoup

#How many per grab
depth = 5

#subreddit to pull from
subreddit = 'worldnews'

#sqlite database
conn = sqlite3.connect('datastore.db')

def get_latest():
	data = urllib.urlopen("http://reddit.com/r/" + subreddit).read()
	return data

def print_links(soup):
	for link in soup.find_all('a'):
		print link.get('href')

def top_links(soup):
	out = []
	for div in soup.find_all('div',attrs={'class':True}):
		l = {"id":'',"rank":0,"title":'',"href":'',"time":''}
		if len(div['class']) > 1 and div['class'][1] == "thing":
			if int(div.span.string) <= depth:
				 for i in div.div:
					if 'unvoted' in i['class']:
						print i.string
						l["rank"] = i.string
				 for i in div.find_all('a', attrs={'tabindex':'1'}):
					print i.string
					l["title"] = i.string
					print i.get('href')
					l["href"] = i.get('href')
					l['time'] = time.time()
					l['id'] = hashlib.sha1(str(random.randint(1,2**128))).hexdigest()
				 out.append(l)
	return out

def init_db():
	c = conn.cursor()
	c.execute('''create table if not exists data(id text, href text, rank text, title text, time text)''')
	conn.commit()

def dict2list(data):
	ll = []
	for k, v in data.iteritems():
		ll.append(v)
	return ll	

def archive(data):
	for l in data:
		os.system("mkdir -p archive")
		os.system("wget " + l['href'] + " -O archive/" + l['id'])

def pulse(data):
	c = conn.cursor()
	for l in data:
		ll = dict2list(l)
		print ll
		c.execute('''insert into data (time, href, id, rank, title) values (?,?,?,?,?)''', ll)
		conn.commit()

init_db()
soup = BeautifulSoup(get_latest())
t = top_links(soup)
pulse(t)
archive(t)

conn.close()
